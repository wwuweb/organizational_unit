<?php
/**
 * @file
 * organizational_unit.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function organizational_unit_taxonomy_default_vocabularies() {
  return array(
    'organizational_unit' => array(
      'name' => 'Organizational Unit',
      'machine_name' => 'organizational_unit',
      'description' => 'Organizational Units are high level groupings such as college, department, or program.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
