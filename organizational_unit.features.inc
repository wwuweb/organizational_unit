<?php
/**
 * @file
 * organizational_unit.features.inc
 */

/**
 * Implements hook_node_info().
 */
function organizational_unit_node_info() {
  $items = array(
    'organizational_unit' => array(
      'name' => t('Organizational Unit'),
      'base' => 'node_content',
      'description' => t('Organizational Units are groups that delegate access to the appropriate group members. '),
      'has_title' => '1',
      'title_label' => t('Organizational Unit'),
      'help' => '',
    ),
  );
  return $items;
}
